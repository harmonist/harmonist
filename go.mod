module codeberg.org/anaseto/harmonist

go 1.17

require (
	codeberg.org/anaseto/gruid v0.23.0
	codeberg.org/anaseto/gruid-js v0.2.0
	codeberg.org/anaseto/gruid-sdl v0.5.0
	codeberg.org/anaseto/gruid-tcell v0.3.0
	github.com/gdamore/tcell/v2 v2.6.0
)

require (
	github.com/gdamore/encoding v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mattn/go-runewidth v0.0.14 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/veandco/go-sdl2 v0.4.35 // indirect
	golang.org/x/image v0.9.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
	golang.org/x/term v0.7.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)

// replace codeberg.org/anaseto/gruid => ../codeberg.org/anaseto/gruid

// replace codeberg.org/anaseto/gruid-sdl => ../codeberg.org/anaseto/gruid-sdl

// replace codeberg.org/anaseto/gruid-tcell => ../codeberg.org/anaseto/gruid-tcell

// replace codeberg.org/anaseto/gruid-js => ../codeberg.org/anaseto/gruid-js
